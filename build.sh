#!/bin/bash

set -e

export INSTALL="$(pwd)/install"
export DOWNLOADS="$(pwd)/downloads"
export DEB_VERSION=4.10.2

mkdir -p $INSTALL
mkdir -p $DOWNLOADS
rm -rf $INSTALL/*

[ -f $DOWNLOADS/slicer.tar.gz ] || wget -O  $DOWNLOADS/slicer.tar.gz "https://download.slicer.org/bitstream/1023242"

cd $INSTALL
mkdir -p opt
cd opt
tar xzvf $DOWNLOADS/slicer.tar.gz
mv Slicer-4.10.2-linux-amd64 Slicer
cp ../../3DSlicer-DesktopIcon.png Slicer/Slicer.png
cd $INSTALL
mkdir -p usr/share/applications
cat <<EOF > usr/share/applications/slicer.desktop
[Desktop Entry]
Type=Application
Name=3DSlicer
Icon=/opt/Slicer/Slicer.png
Exec=env MESA_GL_VERSION_OVERRIDE=3.3FC /opt/Slicer/Slicer
Terminal=false
Categories=Utility;Science;MedicalSoftware;
EOF

cd ..

fpm -s dir -t deb -n slicer -v $DEB_VERSION \
   --description="Slicer - display 3D medical imaging" \
   --after-install after-install.sh \
   --before-remove before-remove.sh \
   -d libpulse-dev \
   -d libnss3 \
   -d libglu1-mesa \
  -C $INSTALL opt/ usr/ 
